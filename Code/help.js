	"use strict"; // force la déclaration des variables
	var Carte, OldInfWindow, Adrs1, Latit, longi, Altit, Marker1, Marker2, MaLigne, dispo, lftl1;
	window.onresize = resizeon;
	window.onload = loadon;
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function loadon()
{
	dispo = extrat(document.getElementById("Pl").style.left) + 
			 extrat(document.getElementById("Pl").style.width) +
			 extrat(document.getElementById("Rs").style.right) + 
			 extrat(document.getElementById("Rs").style.width) + 24;
	//document.getElementById("result").innerHTML = dispo;
	lftl1 = extrat(document.getElementById("Ll").style.left);
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function extrat(don)
{
	var valeur = String(don);
	valeur = valeur.replace('px', '');
	valeur = (valeur * 1);  // *1 convertie en Number
	return valeur;
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function resizeon()
{
	var htf = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	htf = htf - 105;
	document.getElementById("macarte").style.height = htf +'px';
	var wf = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	if (wf >= 1240) {
		var reste = (wf - dispo) / 2;
		reste =  reste.toFixed(0);
		var lftl2 = Number(lftl1) + Number(reste) + Number("6"); 
		document.getElementById("Ll").style.width = reste + 'px';
		document.getElementById("L2").style.left = lftl2 + 'px';
		document.getElementById("L2").style.width = reste + 'px';
	}
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function ChoixMap(value)
{
	switch (value) 
	{
    case 'TERRAIN':
		Carte.setMapTypeId(google.maps.MapTypeId.TERRAIN);
        break;
    case 'ROADMAP':
		Carte.setMapTypeId(google.maps.MapTypeId.ROADMAP);
        break;
    case 'SATELLITE':
		Carte.setMapTypeId(google.maps.MapTypeId.SATELLITE);
        break;
    case 'HYBRID':
		Carte.setMapTypeId(google.maps.MapTypeId.HYBRID);
	}
}	
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function zoomonchange()
{
Carte.setZoom(Number(document.getElementById("zoom").value));
}	
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function zoomonmouseup(event)
{
	var x = event.which || event.keyCode;
	var y = Number(document.getElementById("zoom").value);
	var z = Carte.getZoom();
	if ((x == 2) && (y != z)){Carte.setZoom(y)}; //bouton central de la souris
}	
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function initMap()
{
    var mapCanvas;
	mapCanvas = document.getElementById("macarte");
	//46.66451741754235,6.6066035 magland         48.471110997291696,1.501031219959259 CHAMPHOL          48.0228868,2.724609375 centre france
	var mapOptions = 
		{center: new google.maps.LatLng(48.0228868,2.724609375),
		//disableDefaultUI: true, // cacher ou non tous les controles
		mapTypeControl: false, // cacher ou non le choix des terrains
		zoomControl: false,	 // cacher ou non le zoom
		fullscreenControl: true, // cacher ou non le plein écran
		//streetViewControl: false, // cacher ou non le parcour filmé
		zoom: 6,
		mapTypeId: google.maps.MapTypeId.TERRAIN}; 
	Carte = new google.maps.Map(mapCanvas,mapOptions);
	//Ajouter un écouteur pour l'événement click sur la carte, pour affichage de l'adresse, l'altitude, la latitude et la longitude.
	Carte.addListener('click', function(event){if( !event.placeId){displayLocationElevation(event.latLng,'')}})  
 
	var input = document.getElementById('pacinput');
	var autocomplete = new google.maps.places.Autocomplete(input);
	autocomplete.bindTo('bounds', Carte);
	autocomplete.addListener('place_changed', function() {
    var place = autocomplete.getPlace();
	if (!place.geometry)
	{
		return; // proposition d'un lieu non valide quitte la procedure
    }
    if (place.geometry.viewport)
		{
		} else {
		map.setCenter(place.geometry.location); 
		map.setZoom(14);
		document.getElementById('zoom').selectedIndex = 6;
		}
	displayLocationElevation(place.geometry.location,place.formatted_address);
  });
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function getLocation(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else { 
        alert('Geolocation non supprotée par votre navigateur.');
    }
}		//---------------------------------------------------------
function showPosition(position){
	Carte.panTo(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
	//Carte.setCenter(new google.maps.LatLng(position.coords.latitude,position.coords.longitude));
	Carte.setZoom(12)
	document.getElementById('zoom').selectedIndex = 5;
}		//---------------------------------------------------------
function showError(error) {
    switch(error.code){
        case error.PERMISSION_DENIED:
            alert('Utilisateur a rejeté la demande de géolocalisation.');
            break;
        case error.POSITION_UNAVAILABLE:
            alert('Informations sur la localisation ne sont pas disponibles.');
            break;
        case error.TIMEOUT:
            alert('La demande pour obtenir l’emplacement de l’utilisateur a expiré.');
            break;
        case error.UNKNOWN_ERROR:
            alert('Une erreur inconnue s’est produite.');
            break;
    }
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function Centrer1()
{
	if (document.getElementById("adrss1").innerHTML == '' ) return;
	Carte.setCenter(new google.maps.LatLng(document.getElementById("lat1").innerHTML,document.getElementById("lon1").innerHTML));
	//Carte.setZoom(11)
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function Centrer2()
{
	if (document.getElementById("adrss2").innerHTML == '' ) return;
	Carte.setCenter(new google.maps.LatLng(document.getElementById("lat2").innerHTML,document.getElementById("lon2").innerHTML));
	//Carte.setZoom(11)
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function displayLocationElevation(location,NamePlace)
{
	if (typeof(OldInfWindow) != 'undefined') OldInfWindow.close();// permet de fermer l'infos que j'ai instancié precedement
	var MyArr =  String(location);
	var CvString = MyArr.replace("(",""); CvString = CvString.replace(")","");
	var Tbl = CvString.split(",");
	var latlng = {lat: parseFloat(Tbl[0]), lng: parseFloat(Tbl[1])};
	var Latitude = Math.round(Tbl[0]*100)/100;
		Latit = Tbl[0];
	var Longitude = Math.round(Tbl[1]*100)/100;
		longi = Tbl[1];
	var InfText1, InfText2;
	//------------------------------------------------------------------------------------------------------------
	if (NamePlace != '')
		{
		InfText1 = NamePlace;
		Adrs1 = InfText1;
		ActuAffichage(InfText1,InfText2,location);
		}else{
		var geocoder = new google.maps.Geocoder;
		//Lancer la requête pour recuperer l'adresse (code postal,commune et pays)
		geocoder.geocode({'location': latlng}, function(results, status)
		{
		if (status === google.maps.GeocoderStatus.OK)
			{
			if (results[1]) //Récupérer le deuxieme element 
				{
				CvString = results[1].formatted_address;
				InfText1 = CvString.replace("Unnamed Road,", " ");
				Adrs1 = InfText1;
				ActuAffichage(InfText1,InfText2,location);
				}else{
				alert("Aucune information retournée");
				}
			}else{
			alert("Geocodeur échec dû à: " + status);
			}
		})
	}
	//------------------------------------------------------------------------------------------------------------
	var elevator = new google.maps.ElevationService;
	//Lancer la requête pour recuperer l'altitude
	elevator.getElevationForLocations({'locations': [location]}, function(results, status)
	{
    if (status === google.maps.ElevationStatus.OK)
		{
		if (results[0]) //Récupérer le premier element
			{
			var Altitude = Math.round(results[0].elevation);
			InfText2 = "Altitude: " + Altitude + "M.<br> Latitude: " + Latitude + "°<br> Longitude: " + Longitude + "°";
			Altit = Altitude
			ActuAffichage(InfText1,InfText2,location);
			}else{
			alert("Aucune information retournée")
			}
		}else{
		alert("Echec dû à: " + status);
		}
	})
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function ActuAffichage(Data1,Data2,location)
{
	if ((typeof(Data1) != 'undefined') && (typeof(Data2) != 'undefined'))
		{
		var infowindow = new google.maps.InfoWindow({map: Carte});
		infowindow.setContent("<div style='background-color:lightblue'><p style='color:red'>" + Data1 + "</p></div>"
							+ Data2 + "<br>"
							+ "<input type='button' title='Valider pointage lieux 1' onclick='Affichage1()' value='Lieux 1'>"
							+ "<input type='button' title='Valider pointage lieux 2' onclick='Affichage2()' value='Lieux 2'>");
		infowindow.setPosition(location);
		OldInfWindow = infowindow;
		}
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function Affichage1()
{
	if (typeof(Adrs1) == 'undefined') return;
	if (typeof(MaLigne) != 'undefined') {MaLigne.setMap(null)};
	if (typeof(Marker1) != 'undefined') {Marker1.setMap(null)};
	if (typeof(OldInfWindow) != 'undefined') {OldInfWindow.close()};
 
	document.getElementById("adrss1").innerHTML = Adrs1 + ' Alt. ' + Altit + 'm';
	document.getElementById("lat1").innerHTML = Latit;
	document.getElementById("lon1").innerHTML = longi;
	marquer1();
	if (document.getElementById("adrss2").innerHTML != '' && Marker2.getMap() != null){calculer();}
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function marquer1()	// création du marker1
{    
	if (typeof(Marker1) != 'undefined') {Marker1.setMap(null)};
	Marker1 = new google.maps.Marker({
    'icon' : 'http://maps.google.com/mapfiles/kml/paddle/grn-blank.png', // affiche un marker vert
    'title' : document.getElementById("adrss1").innerHTML,
	'position' : new google.maps.LatLng(document.getElementById("lat1").innerHTML, document.getElementById("lon1").innerHTML),
    'map' : Carte                                                 // l'objet carte sur lequel est affiché le marker
	});
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function Affichage2()
{
	if (typeof(Adrs1) == 'undefined') return;
  	if (typeof(MaLigne) != 'undefined') {MaLigne.setMap(null)};
	if (typeof(Marker2) != 'undefined') {Marker2.setMap(null)};
	if (typeof(OldInfWindow) != 'undefined') {OldInfWindow.close()};
 
	document.getElementById("adrss2").innerHTML = Adrs1 + ' Alt. ' + Altit + 'm';
	document.getElementById("lat2").innerHTML = Latit;
	document.getElementById("lon2").innerHTML = longi;
	marquer2();
	if (document.getElementById("adrss1").innerHTML != '' && Marker1.getMap() != null){calculer();}
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function marquer2()	// création du marker2
{    
 	if (typeof(Marker2) != 'undefined') {Marker2.setMap(null)};
	Marker2 = new google.maps.Marker({
    'icon' : ' http://maps.google.com/mapfiles//kml/paddle/blu-blank.png', // affiche un marker bleu
    'title' : document.getElementById("adrss2").innerHTML,
    'position' : new google.maps.LatLng(document.getElementById("lat2").innerHTML, document.getElementById("lon2").innerHTML),
    'map' : Carte
	});
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function calculer()
{    
	if (document.getElementById("adrss1").innerHTML != '' && document.getElementById("adrss2").innerHTML != '')
		{
		var Den1 = document.getElementById("adrss1").innerHTML.split(" ");
		var Deniv1 = Den1[Den1.length - 1];
		Deniv1 = Number(Deniv1.replace("m",""));
 
		var Den2 = document.getElementById("adrss2").innerHTML.split(" ");
		var Deniv2 = Den2[Den2.length - 1];
		Deniv2 = Number(Deniv2.replace("m",""));
 
		var Deniv
		if (Deniv1 > Deniv2) {Deniv = Deniv1 - Deniv2;}else{Deniv = Deniv2 - Deniv1;}
		var point1 = new google.maps.LatLng(document.getElementById("lat1").innerHTML,
											document.getElementById("lon1").innerHTML);
		var point2 = new google.maps.LatLng(document.getElementById("lat2").innerHTML,
											document.getElementById("lon2").innerHTML);
		var distance = google.maps.geometry.spherical.computeDistanceBetween(point2,point1);
		var conversion = (distance/1000).toFixed(3);//Prendre trois chiffres après la virgule
		document.getElementById("egal1").innerHTML = 'Distance: ' + conversion + 'Km';
		document.getElementById("egal2").innerHTML = 'Dénivelé: ' + Deniv + 'm';
		if (Marker1.getMap() == null) {marquer1()};
		if (Marker2.getMap() == null) {marquer2()};
		creationligne();
		}
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function creationligne()// création de la ligne reliant le marker1 au marker2
{    
  	if (typeof(MaLigne) != 'undefined') {MaLigne.setMap(null)};
	var flightPlanCoordinates = [
		new google.maps.LatLng(document.getElementById("lat1").innerHTML, document.getElementById("lon1").innerHTML),
		new google.maps.LatLng(document.getElementById("lat2").innerHTML, document.getElementById("lon2").innerHTML)
        ];
	MaLigne = new google.maps.Polyline({
          path: flightPlanCoordinates,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });
	MaLigne.setMap(Carte);
}
//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*
function effacer()
{    
	MaLigne.setMap(null);Marker1.setMap(null);Marker2.setMap(null);
}